hydra-el (0.15.0-4) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 22:15:18 +0900

hydra-el (0.15.0-3) unstable; urgency=medium

  * Team upload
  * Rebuild with dh-elpa 2.x

 -- David Bremner <bremner@debian.org>  Mon, 25 Jan 2021 08:00:36 -0400

hydra-el (0.15.0-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Thu, 29 Aug 2019 07:20:43 -0300

hydra-el (0.15.0-1) unstable; urgency=medium

  * New upstream version 0.15.0
  * Package hydra and lv as separate packages
  * Migrate to debhelper 12 without d/compat
  * d/control: Declare Standards-Version 4.4.0 (no changes needed)
  * d/copyright: Bump copyright years
  * Remove obsolete patch for tests
  * Refresh patch to clean documentation
  * Add new patch to add version number to lv.el
  * Add README to explain where to find hydra-examples.el

 -- Lev Lamberov <dogsleg@debian.org>  Wed, 10 Jul 2019 20:59:25 +0500

hydra-el (0.14-3) unstable; urgency=medium

  * Add patch to fix tests for Emacs 26 (Closes: #916808)
  * Migrate to dh 11
  * d/control: Update Maintainer (to Debian Emacsen team)
  * d/control: Declare Standards-Version 4.3.0 (no changes needed)
  * d/control: Add Rules-Requires-Root: no
  * d/control: Drop emacs24 and emacs25 from Enhances
  * d/copyright: Bump copyright years
  * d/rules: Drop --parallel

 -- Lev Lamberov <dogsleg@debian.org>  Tue, 01 Jan 2019 14:46:16 +0500

hydra-el (0.14-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with dh-elpa 1.13 to fix byte-compilation with unversioned
    emacs

 -- David Bremner <bremner@debian.org>  Sat, 02 Jun 2018 20:55:09 -0300

hydra-el (0.14-1) unstable; urgency=medium

  * New upstream version 0.14
  * Clean d/control: remove hardcoded dependency on emacsen-common and
    Built-Using field

 -- Lev Lamberov <dogsleg@debian.org>  Wed, 26 Jul 2017 18:47:38 +0500

hydra-el (0.13.6-2) unstable; urgency=medium

  [ Nicholas D Steeves ]
  * Make dependencies comply with Debian Emacs Policy.
    - debian/control: Depend on emacsen-common (>= 2.0.8) instead of emacs.
  * debian/control: Bump Standards-Version to 4.0.0 (no changes needed).

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 13 Jul 2017 17:55:08 +0500

hydra-el (0.13.6-1) unstable; urgency=low

  * Initial release (Closes: #843075)

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 3 Nov 2016 18:50:41 +0500
